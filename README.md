ansible_role_networking
=========

This role manages Cisco Nexus devices. The role was designed with a specific Nexus deployment in mind. Therefore, not all of the capabilities of the Nexus switches are currently covered.

## Requirements

 - Ansible 2.8

## Usage

This role fully configures a Cisco NXOS based switch from provided variables files and one template. The template sets most of the global configuration, such as the local users, snmp users, stp configuration etc. Upon execution, the role will create a config checkpoint on target device's bootflash:, run through all the tasks, save the running config to the user's home directory and the startup config.

### Role Variables

This role utilizes variables from three distinct variable files:

sys_req_vars.yml
```
nw_sys_req_vars:

  building: "1"
  room: "1"
  rack: "1"
  role: "CORE"
  subrole: "PRI"
  protocol: "tftp"
  bkp_srvr_ip: "192.168.2.254"
  ...
```

nw_int_vars.yml
```
portchannel_list:
  - group: "1"
    members: [ Ethernet1/7, Ethernet1/8 ]
    force: "yes"
    mode: "active"
    state: "present"
```
```
nw_int_list:
  - name: "Ethernet1/1"
    description:
      link_type: "vPC"
      building: "B2"
      room: "R2"
      rack: "RR2"
      device_type: "TOR"
      interface: "Te1/1/1"
    mode:
      layer: "layer2"
      type: "trunk"
      ipv4:
    speed: "10000"
    access_vlan:
    native_vlan: "2"
    trunk_allowed_vlans: "3-4"
    admin_state: "up"
    state: "present"
    channel_group:
      group: "2"
      enabled: true
    peerlink: false
    keepalive: false
```

nw_vlan_vars.yml
```
nw_vlan_list:
  - vlan_id: "3"
    name:
      enclave: "Switch"
      purpose: "Mgmt"
      privacy: "Test"
    vlan_state: "active"
    admin_state: "up"
    state: "present"
    interface:
      description:
        enclave: "Switch"
        purpose: "Management"
        privacy: "(Test)"
      ipv4: "192.168.3.2/24"
      state: "present"
      exists: true
      vrrp:
        group: "1"
        vip: "192.168.3.1"
        admin_state: "no shutdown"
        state: "present"
        enabled: true
```

### Example Playbook

```
---
- name: Configure the Test NX 9k Switch
  hosts: nxos_test_remote
  gather_facts: no

  vars:
    checkpoint: "{{ lookup('pipe', 'date +%Y%m%d-%H%M') }}.cfg"

  tasks:
    - include_role:
        name: ansible_role_networking

```
## Molecule Testing
To test the role with Molecule, provision the virtual environment in DigitalOcean with ansible_role_networking/molecule/droplet_create.yml. The playbook will automatically update the ansible_role_networking/molecule/default/inventories/hosts with the IP address of your newly created Droplet. Use droplet_destroy.yml to deprovision the environment when no longer needed.

droplet_create.yml
(Make sure you paste your DO API Token in the playbook!)
```
---
- name: Create a Droplet in DigitalOcean
  hosts: digitalocean
  gather_facts: no

  vars:
    do_token: #Your DigitalOcean API token

  tasks:
    - name: Provision Ubuntu Droplet with Nexus 9000v VM
      digital_ocean_droplet:
        state: present
      ...
```

hosts
```
[nxos_test_remote]
nxosv ansible_host=x.x.x.x ansible_port=8080

[ubuntu_test]
ubuntu ansible_host=x.x.x.x ansible_user=root ansible_port=22
```

## License

MIT

## Author Information

Dreamer Labs
